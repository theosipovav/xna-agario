﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgarIO
{
    class CamView
    {
        Matrix Transform;
        Viewport View;
        Vector2 Screeen_center;

              
        public CamView(Viewport newViewport)
        {
            View = newViewport;
        }

        public void Update(GameTime gametime, Player cPlayer)
        {
            Vector2 Hero_position = cPlayer.get_v2Position();
            Screeen_center = new Vector2(Hero_position.X - 640, Hero_position.Y - 360);
            Transform = Matrix.CreateScale(new Vector3(1, 1, 0)) * Matrix.CreateTranslation(new Vector3(-Screeen_center.X, -Screeen_center.Y, 0));
        }


        /* Получение private переменных */

        public Matrix get_Transform()
        {
            return Transform;
        }

    }
}
