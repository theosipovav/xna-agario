﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AgarIO
{
    class InputText
    {
        string 
            sSymbol = "Null";
        int 
            nSize = 12;
        SpriteFont 
            spritefontUI;
        Vector2 
            v2Position;
        KeyboardState 
            ksKeyUP, 
            ksKeyDown;
        public string 
            sFullText = "";
        public InputText(Vector2 newPosition, SpriteFont newTText_UI)
        {
            v2Position = newPosition;
            spritefontUI = newTText_UI;
        }
        public void Update(GameTime _InGametime)
        {
            ksKeyUP = ksKeyDown;
            ksKeyDown = Keyboard.GetState();
            if (ksKeyDown.IsKeyDown(Keys.Q) && ksKeyUP.IsKeyUp(Keys.Q)) sSymbol = "Й";
            if (ksKeyDown.IsKeyDown(Keys.W) && ksKeyUP.IsKeyUp(Keys.W)) sSymbol = "Ц";
            if (ksKeyDown.IsKeyDown(Keys.E) && ksKeyUP.IsKeyUp(Keys.E)) sSymbol = "У";
            if (ksKeyDown.IsKeyDown(Keys.R) && ksKeyUP.IsKeyUp(Keys.R)) sSymbol = "К";
            if (ksKeyDown.IsKeyDown(Keys.T) && ksKeyUP.IsKeyUp(Keys.T)) sSymbol = "Е";
            if (ksKeyDown.IsKeyDown(Keys.Y) && ksKeyUP.IsKeyUp(Keys.Y)) sSymbol = "Н";
            if (ksKeyDown.IsKeyDown(Keys.U) && ksKeyUP.IsKeyUp(Keys.U)) sSymbol = "Г";
            if (ksKeyDown.IsKeyDown(Keys.I) && ksKeyUP.IsKeyUp(Keys.I)) sSymbol = "Ш";
            if (ksKeyDown.IsKeyDown(Keys.O) && ksKeyUP.IsKeyUp(Keys.O)) sSymbol = "Щ";
            if (ksKeyDown.IsKeyDown(Keys.P) && ksKeyUP.IsKeyUp(Keys.P)) sSymbol = "З";
            if (ksKeyDown.IsKeyDown(Keys.A) && ksKeyUP.IsKeyUp(Keys.A)) sSymbol = "Ф";
            if (ksKeyDown.IsKeyDown(Keys.S) && ksKeyUP.IsKeyUp(Keys.S)) sSymbol = "Ы";
            if (ksKeyDown.IsKeyDown(Keys.D) && ksKeyUP.IsKeyUp(Keys.D)) sSymbol = "В";
            if (ksKeyDown.IsKeyDown(Keys.F) && ksKeyUP.IsKeyUp(Keys.F)) sSymbol = "А";
            if (ksKeyDown.IsKeyDown(Keys.G) && ksKeyUP.IsKeyUp(Keys.G)) sSymbol = "П";
            if (ksKeyDown.IsKeyDown(Keys.H) && ksKeyUP.IsKeyUp(Keys.H)) sSymbol = "Р";
            if (ksKeyDown.IsKeyDown(Keys.J) && ksKeyUP.IsKeyUp(Keys.J)) sSymbol = "О";
            if (ksKeyDown.IsKeyDown(Keys.K) && ksKeyUP.IsKeyUp(Keys.K)) sSymbol = "Л";
            if (ksKeyDown.IsKeyDown(Keys.L) && ksKeyUP.IsKeyUp(Keys.L)) sSymbol = "Д";
            if (ksKeyDown.IsKeyDown(Keys.Z) && ksKeyUP.IsKeyUp(Keys.Z)) sSymbol = "Я";
            if (ksKeyDown.IsKeyDown(Keys.X) && ksKeyUP.IsKeyUp(Keys.X)) sSymbol = "Ч";
            if (ksKeyDown.IsKeyDown(Keys.C) && ksKeyUP.IsKeyUp(Keys.C)) sSymbol = "С";
            if (ksKeyDown.IsKeyDown(Keys.V) && ksKeyUP.IsKeyUp(Keys.V)) sSymbol = "М";
            if (ksKeyDown.IsKeyDown(Keys.B) && ksKeyUP.IsKeyUp(Keys.B)) sSymbol = "И";
            if (ksKeyDown.IsKeyDown(Keys.N) && ksKeyUP.IsKeyUp(Keys.N)) sSymbol = "Т";
            if (ksKeyDown.IsKeyDown(Keys.M) && ksKeyUP.IsKeyUp(Keys.M)) sSymbol = "Ь";
            if (ksKeyDown.IsKeyDown(Keys.OemOpenBrackets) && ksKeyUP.IsKeyUp(Keys.OemOpenBrackets)) sSymbol = "Х";
            if (ksKeyDown.IsKeyDown(Keys.OemCloseBrackets) && ksKeyUP.IsKeyUp(Keys.OemCloseBrackets)) sSymbol = "Ъ";
            if (ksKeyDown.IsKeyDown(Keys.OemComma) && ksKeyUP.IsKeyUp(Keys.OemComma)) sSymbol = "Б";
            if (ksKeyDown.IsKeyDown(Keys.OemPeriod) && ksKeyUP.IsKeyUp(Keys.OemPeriod)) sSymbol = "Ю";
            if (ksKeyDown.IsKeyDown(Keys.OemSemicolon) && ksKeyUP.IsKeyUp(Keys.OemSemicolon)) sSymbol = "Ж";
            if (ksKeyDown.IsKeyDown(Keys.OemQuotes) && ksKeyUP.IsKeyUp(Keys.OemQuotes)) sSymbol = "Э";
            if (ksKeyDown.IsKeyDown(Keys.Back) && ksKeyUP.IsKeyUp(Keys.Back)) if (sFullText.Length != 0) sFullText = sFullText.Substring(0, sFullText.Length - 1);
            if ((sSymbol != "Null") && (sFullText.Length < nSize))
            {
                sFullText = sFullText + sSymbol;
                sSymbol = "Null";
            }
        }
        public void Draw(SpriteBatch _SpriteBatch)
        {
            Color _Color = new Color();
            if (sFullText.Length >= nSize) _Color = Color.Red;
            else _Color = Color.DarkGreen;
            _SpriteBatch.DrawString(spritefontUI, sFullText, v2Position, _Color);
        }
        public void Delete_FullText()
        {
            sFullText = "";
        }
    }
}
