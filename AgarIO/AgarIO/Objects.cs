﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgarIO
{
    class Objects
    {
        protected Texture2D
            t2dTexture;         // Текстура
        protected Rectangle
            rRect,              // Прямоугольник для отрисовки
            rRectInt;           // Прямоугольник ждя взаимодействия с другими объектами
        protected Vector2
            v2Position,         // Вектор позиции
            v2PositionOriginal, // Вектор оригинальной позиции
            v2Velosity;         // Вектор двжиения
        protected Color
            colorObject;        // Цвет объекта
        protected int
            nMass;              // Масса объекта
        protected float
            fSizeObj;           // Размер объекта


        public Objects(Texture2D new_t2dTexture, Vector2 new_v2Position)
        {
            t2dTexture = new_t2dTexture;
            v2Position = new_v2Position;
            nMass = 1;
            colorObject = new Color(0, 100, 255, 255);

        }

        public void Update(GameTime gameTime)
        {
            rRect = new Rectangle((int)v2Position.X, (int)v2Position.Y, t2dTexture.Width, t2dTexture.Height);
            rRectInt = new Rectangle();
            rRectInt.X = (int)v2Position.X - (int)((t2dTexture.Width * fSizeObj) / 2);
            rRectInt.Y = (int)v2Position.Y - (int)(t2dTexture.Height * fSizeObj / 2);
            rRectInt.Width = (int)(t2dTexture.Width * fSizeObj);
            rRectInt.Height = (int)(t2dTexture.Height * fSizeObj);
            int W = rRectInt.Width / 100 * 30;
            int H = rRectInt.Height / 100 * 30;
            rRectInt.X += W;
            rRectInt.Width -= W * 2;
            rRectInt.Y += H;
            rRectInt.Height -= H * 2;
            //rRectInt = new Rectangle((int)v2Position.X - (int)((250 * fSizeObj) / 2), (int)v2Position.Y - (int)(250 * fSizeObj / 2), (int)(250 * fSizeObj), (int)(250 * fSizeObj));
            v2PositionOriginal = new Vector2(rRect.Width / 2, rRect.Height / 2);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(t2dTexture, v2Position, new Rectangle(0, 0, t2dTexture.Width, t2dTexture.Height), colorObject, 0f, v2PositionOriginal, fSizeObj, SpriteEffects.None, 0.7f);
        }

        /* Получение private переменных */
        public Rectangle get_rRectInt()
        {
            return rRectInt;
        }

    }

    class Pointer : Objects
    {
        bool
            isActive;               // Статус объекта
        SpriteFont
            spritefontUI;           // Шрифт для интерфейса
        

        public Pointer(Texture2D new_t2dTexture, Vector2 new_v2Position, SpriteFont new_spritefontUI) : base(new_t2dTexture, new_v2Position)
        {
            spritefontUI = new_spritefontUI;
            isActive = true;
        }

        public void Update(GameTime gameTime)
        {
            // Если объект "съеден" (isActive == false) то данный метод завершается
            if (!isActive) return;

            // Определение размера
            fSizeObj = nMass * 0.1f;

            if (v2Position.X < -2260)
            {
                // При достижение левого края карты меняется вектор движения
                v2Velosity.X *= (-1);
            }
            if (v2Position.X > 1560)
            {
                // При достижение правого края карты меняется вектор движения
                v2Velosity.X *= (-1);
            }
            if (v2Position.Y < -2505)
            {
                // При достижение верхнего края карты меняется вектор движения
                v2Velosity.Y *= (-1);
            }
            if (v2Position.Y > 1805)
            {
                // При достижение нижнего края карты меняется вектор движения
                v2Velosity.Y *= (-1);
            }
            // Движение объекта
            v2Position += v2Velosity;

            base.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            // Если объект "съеден" (isActive == false) то данный метод завершается
            if (!isActive) return;



            if (nMass.ToString().Length == 1)
            {
                if (nMass < 5)
                {
                    spriteBatch.DrawString(spritefontUI, nMass.ToString(), v2Position + new Vector2(-6, -14), Color.Black, 0f, new Vector2(0, 0), 0.7f, SpriteEffects.None, 0.49f);
                }
                else
                {
                    spriteBatch.DrawString(spritefontUI, nMass.ToString(), v2Position + new Vector2(-8, -19), Color.Black, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 0.49f);

                }
            }
            else
            {
                spriteBatch.DrawString(spritefontUI, nMass.ToString(), v2Position + new Vector2(-12, -19), Color.Black, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 0.49f);

            }

            base.Draw(spriteBatch);
        }

        /* Вспомогательные функции */
        public void Delete()
        {
            // Объект "съеден" > isActive == false
            isActive = false;
        }
        public void SetVectorMotion(int nMode)
        {
            // Установка вектора движения
            switch (nMode)
            {
                case 0:
                    v2Velosity = new Vector2(-3, -3);
                    break;
                case 1:
                    v2Velosity = new Vector2(3, -3);
                    break;
                case 2:
                    v2Velosity = new Vector2(-3, -3);
                    break;
                case 3:
                    v2Velosity = new Vector2(-3, 3);
                    break;
                default:
                    v2Velosity = new Vector2(0, 0);
                    break;
            }
        }
        public void SetMass(int nValue)
        {
            // Установка массы
            nMass = nValue;
        }
        public void SetColour(Color new_colorObject)
        {
            colorObject = new_colorObject;
        }

        /* Получение private переменных */
        public bool get_isActive()
        {
            return isActive;
        }
        public int get_nMass()
        {
            return nMass;
        }
    }

    class Trap : Objects
    {
        public Trap(Texture2D new_t2dTexture, Vector2 new_v2Position) : base(new_t2dTexture, new_v2Position)
        {
            colorObject = new Color(255, 255, 255, 255);
            fSizeObj = 0.5f;
        }

        public void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }
    }

    class Bonus : Objects
    {
        bool
            isActive;               // Статус объекта
        float
            fTimerAnimation;

        public Bonus(Texture2D new_t2dTexture, Vector2 new_v2Position) : base(new_t2dTexture, new_v2Position)
        {
            fTimerAnimation = 0;
            isActive = true;
            colorObject = new Color(255, 255, 255, 255);
            fSizeObj = 0.5f;

        }

        public void Update(GameTime gameTime)
        {
            // Если объект "съеден" (isActive == false) то данный метод завершается
            if (!isActive) return;

            // Реализация анимации
            
            
            fTimerAnimation += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (fTimerAnimation >= 500)
            {
                if (fSizeObj == 0.5f) { fSizeObj = 0.7f; }
                else { fSizeObj = 0.5f; }
                fTimerAnimation = 0;
            }

            base.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (!isActive) return;
            base.Draw(spriteBatch);
        }

        /* Вспомогательные функции */
        public void Delete()
        {
            // Объект "съеден" > isActive == false
            isActive = false;
        }

        /* Получение private переменных */
        public bool get_isActive()
        {
            return isActive;
        }
    }
}
