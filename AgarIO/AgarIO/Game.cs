using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace AgarIO
{

    public class Game : Microsoft.Xna.Framework.Game
    {
        /* ��������� ���������� */
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        /* ���������������� ���������� */
        int nStageGame,         // ����� ���� (0 - ����, 1 - ����, 3 - ����� ����)
            nStageMenu,         // ����� ���� (0 - ���� �����, 1 - �������)
            nStageLvl,          // ����� ������� (0 - 1 ��., 1 - 2 ��.)
            nStageFinal,        // �������� ������ (0 - ������, 1 - ���������)
            nPointerAll;          // ����� ��������� �����
        int[]
            aPointerEnd;        // ����������� ���-�� ���� ��� ������ �� 1 ��� 2 ��.
        float[] 
            aTimerLvl;          // ������ ��� ��.
        Random 
            rndRandomValue;
        Player 
            cPlayer;                    // ����� �������� �������� ���������
        CamView 
            cCV;                        // ����� ����������������� ������ (������ ������������� � ��)
        List<Pointer>
            listPointer_lvl1,           // ������ ������� ������� �������� (�����������) ��� 1 ��.
            listPointer_lvl2;           // ������ ������� ������� �������� (�����������) ��� 2 ��.
        List<Bonus>
            listBonus_AddLife;          // ������ ������� ������� �������� (�������) ��� 2 ��.
        List<Trap>
            listTraps;                  // ������ ������� ������� �������� (�������) ��� 2 ��.
        Buttons[] 
            _Buttons;                   // ������ ������� ������ ��� ����
        InputText 
            _InputText;                 // ����� ��������� ����� ������
        MouseState
            msMouse;                    // ����� ��� ������������ �������� ����
        KeyboardState
            ksKeys;                     // ����� ��� ������������ �������� ����������
        Song[]
            songMusic;                  // ������ ��� ����
        bool[]
            isFlagMusic;                // ���� ��� ���/���� ������ 
        public Game()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            IsMouseVisible = true;
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            graphics.ApplyChanges();
            base.Initialize();
        }


        protected override void LoadContent()
        {
            /* �������� ���. �������� ��� ������� �������� (������� ��� ������� ������� � �������, �������� �������, ����������� � ������ ��� ����)
             * ��� ��, � ����������� �� ������������� �������, ��� �� ��� �������� ��������� ���������� �����������������
             */
            spriteBatch = new SpriteBatch(GraphicsDevice);
            rndRandomValue = new Random();
            // ������������� ����������������� ������
            cCV = new CamView(GraphicsDevice.Viewport);
            // ������������� ������
            _Buttons = new Buttons[7];
            _Buttons[0] = new Buttons(Content.Load<Texture2D>("Btn1"), new Vector2(300, 300));
            _Buttons[1] = new Buttons(Content.Load<Texture2D>("Btn2"), new Vector2(300, 400));
            _Buttons[2] = new Buttons(Content.Load<Texture2D>("Btn3"), new Vector2(300, 500));
            _Buttons[3] = new Buttons(Content.Load<Texture2D>("Btn4"), new Vector2(1050, 630));
            _Buttons[4] = new Buttons(Content.Load<Texture2D>("Btn5"), new Vector2(1000, 300));
            _Buttons[5] = new Buttons(Content.Load<Texture2D>("Btn6"), new Vector2(1000, 400));
            _Buttons[6] = new Buttons(Content.Load<Texture2D>("Btn7"), new Vector2(1000, 500));
            // ������������� ��������� �����
            _InputText = new InputText(new Vector2(190, 105), Content.Load<SpriteFont>("spritefont_Font3"));
            // ������������� �����
            songMusic = new Song[2];
            songMusic[0] = Content.Load<Song>("SoundMenu");
            songMusic[1] = Content.Load<Song>("SoundPlay");
            isFlagMusic = new bool[2];
            isFlagMusic[0] = false;
            isFlagMusic[1] = false;
            // ������������� ��
            cPlayer = new Player(Content.Load<Texture2D>("Circle"), new Vector2(0, 0), Content.Load<SpriteFont>("spritefontUI"));
            // ������������� �����������
            listPointer_lvl1 = new List<Pointer>();
            for (int iA = 0; iA < 50; iA++)
            {
                Vector2 c2NewPositionPointer = new Vector2(rndRandomValue.Next(-1400, 1400), rndRandomValue.Next(-1500, 1700));
                listPointer_lvl1.Add(new Pointer(Content.Load<Texture2D>("Circle"), c2NewPositionPointer, Content.Load<SpriteFont>("spritefontUI")));
                listPointer_lvl1[listPointer_lvl1.Count - 1].SetVectorMotion(rndRandomValue.Next(0,4));
                listPointer_lvl1[listPointer_lvl1.Count - 1].SetMass(rndRandomValue.Next(1, 15));
                listPointer_lvl1[listPointer_lvl1.Count - 1].SetColour(new Color(rndRandomValue.Next(0, 255), rndRandomValue.Next(0, 255), rndRandomValue.Next(0, 255), 255));
            }
            listPointer_lvl2 = new List<Pointer>();
            for (int iA = 0; iA < 50; iA++)
            {
                Vector2 c2NewPositionPointer = new Vector2(rndRandomValue.Next(-1400, 1400), rndRandomValue.Next(-1500, 1700));
                listPointer_lvl2.Add(new Pointer(Content.Load<Texture2D>("Circle"), c2NewPositionPointer, Content.Load<SpriteFont>("spritefontUI")));
                listPointer_lvl2[listPointer_lvl2.Count - 1].SetVectorMotion(rndRandomValue.Next(0, 4));
                listPointer_lvl2[listPointer_lvl2.Count - 1].SetMass(rndRandomValue.Next(1, 15));
                listPointer_lvl2[listPointer_lvl2.Count - 1].SetColour(new Color(rndRandomValue.Next(0, 255), rndRandomValue.Next(0, 255), rndRandomValue.Next(0, 255), 255));
            }
            // ������������� �������
            listBonus_AddLife = new List<Bonus>();
            for (int iA = 0; iA < 5; iA++)
            {
                Vector2 c2NewPositionBonus = new Vector2(rndRandomValue.Next(-1400, 1400), rndRandomValue.Next(-1500, 1700));
                listBonus_AddLife.Add(new Bonus(Content.Load<Texture2D>("Bonus"), c2NewPositionBonus));
            }
            // ������������� �������
            listTraps = new List<Trap>();
            for (int iA = 0; iA < 5; iA++)
            {
                Vector2 c2NewPositionTrap = new Vector2(rndRandomValue.Next(-1400, 1400), rndRandomValue.Next(-1500, 1700));
                listTraps.Add(new Trap(Content.Load<Texture2D>("Trap"), c2NewPositionTrap));
            }
            // ������������� �������
            aTimerLvl = new float[3];
            // ��� ����������� � UI
            aTimerLvl[0] = 0f;
            // ��� 1 ��.
            aTimerLvl[1] = 60f;
            // ��� 2 ��.
            aTimerLvl[2] = 60f;
            aPointerEnd = new int[3];
            aPointerEnd[0] = 0;
            aPointerEnd[1] = 5;
            aPointerEnd[2] = 15;
            nPointerAll = 0;
            nStageGame = 0;
            nStageMenu = 0;
            nStageLvl = 0;
            nStageFinal = 0;
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            /* ���������� �������� ������ �������������� ������� �������� */
            msMouse = Mouse.GetState();
            ksKeys = Keyboard.GetState();

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed) this.Exit();


            /* ������� */ 
            if (ksKeys.IsKeyDown(Keys.Q))
            {
                /* ����������� � ����� 1 �� � ������� */
                cPlayer.set_nPointer(5);
                aTimerLvl[0] = 5f;
            }
            if (ksKeys.IsKeyDown(Keys.W))
            {
                /* ����������� � ����� 1 �� � ���������� */
                cPlayer.set_nPointer(0);
                aTimerLvl[0] = 5f;
            }
            if (ksKeys.IsKeyDown(Keys.E))
            {
                /* ����������� � ����� 1 �� � ������� */
                cPlayer.set_nPointer(15);
                aTimerLvl[0] = 5f;

            }
            if (ksKeys.IsKeyDown(Keys.R))
            {
                /* ����������� � ����� 1 �� � ������� */
                cPlayer.set_nPointer(0);
                aTimerLvl[0] = 5f;
            }

            switch (nStageGame)
            {
                case 0:
                    /* ���� */
                    // ���������� ������
                    if (isFlagMusic[0] == false)
                    {
                        MediaPlayer.Play(songMusic[0]);
                        MediaPlayer.IsRepeating = true;
                        MediaPlayer.Volume = 0.3f;
                        isFlagMusic[0] = true;
                    }
                    switch (nStageMenu)
                    {
                        case 0:
                            /* �������� ���� */
                            // ������
                            for (int n = 0; n < 3; n++)
                            {
                                _Buttons[n].Update(msMouse);
                            }
                            for (int n = 4; n < 7; n++)
                            {
                                _Buttons[n].Update(msMouse);
                            }


                            if ((_Buttons[0].isClicked == true) || (ksKeys.IsKeyDown(Keys.Enter)))
                            {
                                if ((_InputText.sFullText.Length <= 10) && (_InputText.sFullText.Length > 0))
                                {
                                    cPlayer.set_sName(_InputText.sFullText);
                                    nStageGame = 1;
                                    nStageLvl = 0;
                                    aTimerLvl[0] = aTimerLvl[1];
                                    aPointerEnd[0] = aPointerEnd[1];
                                    cPlayer.Respawn();
                                }
                            }
                            if (_Buttons[1].isClicked == true)
                            {
                                nStageMenu = 1;
                            }
                            if (_Buttons[2].isClicked == true)
                            {
                                Exit();
                            }

                            if (_Buttons[4].isClicked == true)
                            {
                                cPlayer.set_colorPlayer(new Color(0, 255, 0, 255));
                            }
                            if (_Buttons[5].isClicked == true)
                            {
                                cPlayer.set_colorPlayer(new Color(255, 0, 0, 255));
                            }
                            if (_Buttons[6].isClicked == true)
                            {
                                cPlayer.set_colorPlayer(new Color(0, 0, 255, 255));
                            }
                            /* ��������� �������� ����� ����� */
                            _InputText.Update(gameTime);

                            break;
                        case 1:
                            /* ������� */
                            _Buttons[3].Update(msMouse);
                            if (_Buttons[3].isClicked == true)
                            {
                                nStageMenu = 0;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 1:
                    /* ���� */
                    // ���������� ������
                    cCV.Update(gameTime, cPlayer);
                    // ���������� ������
                    if (isFlagMusic[1] == false)
                    {
                        MediaPlayer.Play(songMusic[1]);
                        MediaPlayer.IsRepeating = true;
                        MediaPlayer.Volume = 0.3f;
                        isFlagMusic[1] = true;
                    }
                    // ���������� ��
                    cPlayer.Update(gameTime);
                    // ���������� ������� 1 ��.
                    aTimerLvl[0] -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                    // �������� ������
                    if (cPlayer.get_nLife() <= 0)
                    {
                        nStageGame = 2;
                        nStageFinal = 1;
                    }
                    switch (nStageLvl)
                    {
                        case 0:
                            /* 1 ��. */
                            // �������� �������
                            if (aTimerLvl[0] <= 0)
                            {
                                // ������� �����
                                if (cPlayer.get_nPointer() >= aPointerEnd[0])
                                {
                                    nStageLvl = 1;
                                    aTimerLvl[0] = aTimerLvl[2];
                                    aPointerEnd[0] = aPointerEnd[2];
                                    cPlayer.set_nMass(5);
                                    nPointerAll = cPlayer.get_nPointer();
                                    cPlayer.PonterReset();
                                }
                                else
                                {
                                    nStageGame = 2;
                                    nStageFinal = 1;
                                    nPointerAll = cPlayer.get_nPointer();
                                }
                            }
                            // ���������� ����������� 
                            for (int iC = 0; iC < listPointer_lvl1.Count; iC++)
                            {
                                listPointer_lvl1[iC].Update(gameTime);
                                if (cPlayer.Interaction(listPointer_lvl1[iC].get_rRectInt(), listPointer_lvl1[iC].get_isActive()))
                                {
                                    if (cPlayer.Versus(listPointer_lvl1[iC].get_nMass()))
                                    {
                                        listPointer_lvl1[iC].Delete();
                                    }
                                }
                            }
                            break;
                        case 1:
                            /* 2 ��. */
                            // �������� �������
                            if (aTimerLvl[0] <= 0)
                            {
                                // ������� �����
                                if (cPlayer.get_nPointer() >= aPointerEnd[0])
                                {
                                    nStageGame = 2;
                                    nStageFinal = 0;
                                    nPointerAll += cPlayer.get_nPointer();
                                }
                                else
                                {
                                    nStageGame = 2;
                                    nStageFinal = 1;
                                    nPointerAll += cPlayer.get_nPointer();
                                }
                            }
                            // ���������� ����������� 
                            for (int iC = 0; iC < listPointer_lvl2.Count; iC++)
                            {
                                listPointer_lvl2[iC].Update(gameTime);
                                if (cPlayer.Interaction(listPointer_lvl2[iC].get_rRectInt(), listPointer_lvl2[iC].get_isActive()))
                                {
                                    if (cPlayer.Versus(listPointer_lvl2[iC].get_nMass()))
                                    {
                                        listPointer_lvl2[iC].Delete();
                                    }
                                }
                            }
                            // ���������� ������� 
                            for (int iC = 0; iC < listBonus_AddLife.Count; iC++)
                            {
                                listBonus_AddLife[iC].Update(gameTime);
                                if (cPlayer.Interaction(listBonus_AddLife[iC].get_rRectInt(), listBonus_AddLife[iC].get_isActive()))
                                {
                                    cPlayer.AddLife();
                                    listBonus_AddLife[iC].Delete();
                                }
                            }
                            // ���������� ������� 
                            for (int iC = 0; iC < listTraps.Count; iC++)
                            {
                                listTraps[iC].Update(gameTime);
                                if (cPlayer.Interaction(listTraps[iC].get_rRectInt(), true))
                                {
                                    cPlayer.AddLife();
                                    cPlayer.Respawn();
                                }
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 2:
                    /* ����� */
                    switch (nStageFinal)
                    {
                        case 0:
                            /* ������ */
                            if (ksKeys.IsKeyDown(Keys.Enter))
                            {
                                SaveResult(cPlayer.get_sName(), (nPointerAll + cPlayer.get_nLife() * 2));
                                LoadContent();
                            }
                            break;
                        case 1:
                            /* ��������� */
                            if (ksKeys.IsKeyDown(Keys.Enter))
                            {
                                SaveResult(cPlayer.get_sName(), nPointerAll);
                                LoadContent();
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            /* ���������� �������� ������ ����������� �������
             * 
             */
            GraphicsDevice.Clear(Color.WhiteSmoke);
            switch (nStageGame)
            {
                case 0:
                    /* ���� */
                    spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null);

                    switch (nStageMenu)
                    {
                        case 0:
                            spriteBatch.Draw(Content.Load<Texture2D>("Bg_menu"), new Rectangle(0, 0, 1280, 720), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                            spriteBatch.Draw(Content.Load<Texture2D>("Bg_input"), new Rectangle(50, 50, 744, 169), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.89f);
                            /* �������� ���� */
                            for (int n = 0; n < 3; n++)
                            {
                                _Buttons[n].Draw(spriteBatch);
                            }
                            for (int n = 4; n < 7; n++)
                            {
                                _Buttons[n].Draw(spriteBatch);
                            }
                            _InputText.Draw(spriteBatch);
                            break;
                        case 1:
                            /* ������� */
                            LoadResult();
                            spriteBatch.Draw(Content.Load<Texture2D>("Bg_raiting"), new Rectangle(0, 0, 1280, 720), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                            _Buttons[3].Draw(spriteBatch);
                            break;
                        default:
                            break;
                    }
                    spriteBatch.End();
                    break;
                case 1:
                    /* ���� */
                    spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, cCV.get_Transform());
                    cPlayer.Draw(spriteBatch);
                    switch (nStageLvl)
                    {
                        case 0:
                            /* 1 ��. */
                            // ���������� ���� ��� 1 ��.  
                            DrawBG(Content.Load<Texture2D>("bg_lvl1"));
                            // ���������� ����������� ��� 1 ��.  
                            for (int iC = 0; iC < listPointer_lvl1.Count; iC++)
                            {
                                listPointer_lvl1[iC].Draw(spriteBatch);
                            }
                            break;
                        case 1:
                            /* 2 ��. */
                            // ���������� ���� ��� 2 ��.  
                            DrawBG(Content.Load<Texture2D>("bg_lvl2"));
                            // ���������� ����������� ��� 2 ��.  
                            for (int iC = 0; iC < listPointer_lvl2.Count; iC++)
                            {
                                listPointer_lvl2[iC].Draw(spriteBatch);
                            }

                            // ���������� ������� ��� 2 ��.  
                            for (int iC = 0; iC < listBonus_AddLife.Count; iC++)
                            {
                                listBonus_AddLife[iC].Draw(spriteBatch);
                            }

                            // ���������� ������� ��� 2 ��.  
                            for (int iC = 0; iC < listTraps.Count; iC++)
                            {
                                listTraps[iC].Draw(spriteBatch);
                            }
                            break;
                        default:
                            break;
                    }
                    spriteBatch.End();
                    /* UI */
                    spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null);
                    spriteBatch.DrawString(Content.Load<SpriteFont>("spritefontUI"), "���: " + cPlayer.get_sName(), new Vector2(5, 10), Color.Black, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 0.1f);
                    spriteBatch.DrawString(Content.Load<SpriteFont>("spritefontUI"), "�����: " + cPlayer.get_nLife(), new Vector2(5, 40), Color.Black, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 0.1f);
                    spriteBatch.DrawString(Content.Load<SpriteFont>("spritefontUI"), "�����: " + cPlayer.get_nMass(), new Vector2(5, 70), Color.Black, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 0.1f);
                    spriteBatch.DrawString(Content.Load<SpriteFont>("spritefontUI"), "����: " + cPlayer.get_nPointer(), new Vector2(5, 100), Color.Black, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 0.1f);
                    spriteBatch.DrawString(Content.Load<SpriteFont>("spritefont_Font2"), Math.Round(aTimerLvl[0], 1).ToString(), new Vector2(1150, 10), Color.Red, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 0.1f);
                    if (cPlayer.get_nPointer() >= aPointerEnd[0])
                    {
                        spriteBatch.DrawString(Content.Load<SpriteFont>("spritefontUI"), "������ !", new Vector2(graphics.PreferredBackBufferWidth - 230, graphics.PreferredBackBufferHeight - 50), Color.Black, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 0.1f);
                    }
                    else
                    {
                        spriteBatch.DrawString(Content.Load<SpriteFont>("spritefontUI"), "�������: " + cPlayer.get_nPointer() + " �� " + aPointerEnd[0], new Vector2(graphics.PreferredBackBufferWidth - 230, graphics.PreferredBackBufferHeight - 50), Color.Black, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 0.1f);
                    }
                    spriteBatch.End();
                    break;
                case 2:
                    /* ����� */
                    spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null);
                    switch (nStageFinal)
                    {
                        case 0:
                            /* ������ */
                            spriteBatch.Draw(Content.Load<Texture2D>("Bg_win"), new Rectangle(0, 0, 1280, 720), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);

                            break;
                        case 1:
                            /* ��������� */
                            spriteBatch.Draw(Content.Load<Texture2D>("Bg_loss"), new Rectangle(0, 0, 1280, 720), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);

                            break;
                        default:
                            break;
                    }
                    spriteBatch.End();
                    break;
                default:
                    break;
            }
            base.Draw(gameTime);
        }
        /* ��������������� ������� */
        void DrawBG(Texture2D t2dTextureBackground)
        {
            /* ��������� ���� ��� 1 � 2 ������
             * �� 0x: �� -1400 �� 1400
             * �� 0y: �� -1400 �� 1400
             */
            for (int iy = -2100; iy < 2100; iy += 700)
            {
                for (int ix = -2100; ix < 2100; ix += 700)
                {
                    spriteBatch.Draw(t2dTextureBackground, new Rectangle(ix, iy, 700, 700), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.99f);
                }
            }
            for (int iA = -2100; iA < 2100; iA += 700)
            {
                spriteBatch.Draw(Content.Load<Texture2D>("obj_1"), new Rectangle(iA, -2505, 700, 700), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.81f);
                spriteBatch.Draw(Content.Load<Texture2D>("obj_3"), new Rectangle(iA, 1805, 700, 700), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.81f);
                spriteBatch.Draw(Content.Load<Texture2D>("obj_2"), new Rectangle(1560, iA, 700, 700), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.8f);
                spriteBatch.Draw(Content.Load<Texture2D>("obj_4"), new Rectangle(-2260, iA, 700, 700), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.8f);
            }
        }
        /* ���������� ����������� */
        void SaveResult(string _PlayerName, int _PlayerXP)
        {
            string _PlayerDate = DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
            string _CurrentDirectory = Environment.CurrentDirectory;
            FileStream _FileStream;
            XmlDocument _XmlDocument;
            if (File.Exists(_CurrentDirectory + @"\Save.xml"))
            {
                _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.Open);
                _XmlDocument = new XmlDocument();
                _XmlDocument.Load(_FileStream);
                _FileStream.Close();
                _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.Create);
                if (_XmlDocument["Game"].ChildNodes.Count <= 10)
                {
                    XmlNode _XmlNode = _XmlDocument.CreateElement("Save");
                    XmlAttribute _XmlAttribute1 = _XmlDocument.CreateAttribute("Name");
                    _XmlAttribute1.Value = _PlayerName;
                    _XmlNode.Attributes.Append(_XmlAttribute1);
                    XmlAttribute _XmlAttribute2 = _XmlDocument.CreateAttribute("XP");
                    _XmlAttribute2.Value = _PlayerXP.ToString();
                    _XmlNode.Attributes.Append(_XmlAttribute2);
                    XmlAttribute _XmlAttribute3 = _XmlDocument.CreateAttribute("Date");
                    _XmlAttribute3.Value = _PlayerDate;
                    _XmlNode.Attributes.Append(_XmlAttribute3);
                    _XmlDocument.DocumentElement.AppendChild(_XmlNode);
                }
                else
                {
                    _XmlDocument["Game"].RemoveAll();
                    XmlNode _XmlNode = _XmlDocument.CreateElement("Save");
                    XmlAttribute _XmlAttribute1 = _XmlDocument.CreateAttribute("Name");
                    _XmlAttribute1.Value = _PlayerName;
                    _XmlNode.Attributes.Append(_XmlAttribute1);
                    XmlAttribute _XmlAttribute2 = _XmlDocument.CreateAttribute("XP");
                    _XmlAttribute2.Value = _PlayerXP.ToString();
                    _XmlNode.Attributes.Append(_XmlAttribute2);
                    XmlAttribute _XmlAttribute3 = _XmlDocument.CreateAttribute("Date");
                    _XmlAttribute3.Value = _PlayerDate;
                    _XmlNode.Attributes.Append(_XmlAttribute3);
                    _XmlDocument.DocumentElement.AppendChild(_XmlNode);
                }
                _XmlDocument.Save(_FileStream);
                _FileStream.Close();
            }
            else
            {
                _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.CreateNew);
                _XmlDocument = new XmlDocument();
                _XmlDocument.LoadXml("<Game></Game>");
                XmlNode _XmlNode = _XmlDocument.CreateElement("Save");
                XmlAttribute _XmlAttribute1 = _XmlDocument.CreateAttribute("Name");
                _XmlAttribute1.Value = _PlayerName;
                _XmlNode.Attributes.Append(_XmlAttribute1);
                XmlAttribute _XmlAttribute2 = _XmlDocument.CreateAttribute("XP");
                _XmlAttribute2.Value = _PlayerXP.ToString();
                _XmlNode.Attributes.Append(_XmlAttribute2);
                XmlAttribute _XmlAttribute3 = _XmlDocument.CreateAttribute("Date");
                _XmlAttribute3.Value = _PlayerDate;
                _XmlNode.Attributes.Append(_XmlAttribute3);
                _XmlDocument.DocumentElement.AppendChild(_XmlNode);
                _XmlDocument.Save(_FileStream);
                _FileStream.Close();
            }
        }
        /* �������� ����������� */
        void LoadResult()
        {
            string _CurrentDirectory = Environment.CurrentDirectory;
            if (File.Exists(_CurrentDirectory + @"\Save.xml"))
            {
                string _PlayerName = "";
                string _PlayerXP = "";
                string _PlayerDate = "";
                int _interval = 50;
                FileStream _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.Open);
                XmlDocument _XmlDocument = new XmlDocument();
                _XmlDocument.Load(_FileStream);
                _FileStream.Close();
                for (int i = 0; i < _XmlDocument["Game"].ChildNodes.Count; i++)
                {
                    _PlayerName = _XmlDocument["Game"].ChildNodes[i].Attributes["Name"].InnerText;
                    _PlayerXP = _XmlDocument["Game"].ChildNodes[i].Attributes["XP"].InnerText;
                    _PlayerDate = _XmlDocument["Game"].ChildNodes[i].Attributes["Date"].InnerText;
                    spriteBatch.DrawString(Content.Load<SpriteFont>("spritefont_FontRaiting"), _PlayerName, new Vector2(100, 70 + _interval), Color.Black);
                    spriteBatch.DrawString(Content.Load<SpriteFont>("spritefont_FontRaiting"), _PlayerXP, new Vector2(550, 70 + _interval), Color.Black);
                    spriteBatch.DrawString(Content.Load<SpriteFont>("spritefont_FontRaiting"), _PlayerDate, new Vector2(900, 70 + _interval), Color.Black);
                    _interval += 30;
                }
            }
        }
    }
}