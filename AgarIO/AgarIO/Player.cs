﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgarIO
{
    class Player
    {
        Texture2D
            t2dTexture;             // Текстура
        Rectangle
            rRect,                  // Прямоугольник для отрисовки  
            rRectInt;               // Прямоугольник ждя взаимодействия с другими объектами
        Vector2
            v2Position,             // Вектор позиции
            v2PositionOriginal,     // Вектор оригинальной позиции
            v2Velosity,             // Вектор двжиения
            v2UI;
        Color
            colorPlayer,            // Цвет объекта
            colorPlayerSave;
        KeyboardState
            ksKeys;                 // Кнопки клавиатуры
        MouseState
            msMouse;                // Кнопки и курсор мыши
        SpriteFont
            spritefontUI;           // Шрифт для интерфейса
        string
            sName;                  // Имя игрока
        int
            nMass,                  // Масса объекта
            nLife,                  // Кол-во жизней
            nPointer;               // Кол-во "съеденных"
        float
            fSizePlayer,            // Размер объекта
            fTimetImmunity;         // Таймер иммунитета
        bool
            isImmunity,             // Флаг иммунитета
            isImmunityColourDown;   // Флаг для мигания объекта

        public Player(Texture2D new_t2dTexture, Vector2 new_v2Position, SpriteFont new_spritefontUI)
        {
            t2dTexture = new_t2dTexture;
            v2Position = new_v2Position;
            spritefontUI = new_spritefontUI;
            colorPlayer = new Color(0, 255, 0, 255);
            colorPlayerSave = colorPlayer;
            v2Velosity = new Vector2(5, 5);
            nMass = 5;
            sName = "Игрок";
            v2UI = new Vector2(v2Position.X, v2Position.Y);
            nLife = 5;
            isImmunity = true;
            fTimetImmunity = 0;
            nPointer = 0;
        }

        public void Update(GameTime gameTime)
        {
            // Реализация логики объекта

            // Движение объекта
            ksKeys = Keyboard.GetState();
            msMouse = Mouse.GetState();
            if (ksKeys.IsKeyDown(Keys.Up))
            {
                if (v2Position.Y > (-1825 + fSizePlayer * 125))
                {
                    v2Position.Y -= v2Velosity.Y;
                }
            }
            if (ksKeys.IsKeyDown(Keys.Right))
            {
                if (v2Position.X < (1575 - fSizePlayer * 125))
                {
                    v2Position.X += v2Velosity.X;
                }
            }
            if (ksKeys.IsKeyDown(Keys.Down))
            {
                if (v2Position.Y < (1825 - fSizePlayer * 125))
                {
                    v2Position.Y += v2Velosity.Y;
                }
            }
            if (ksKeys.IsKeyDown(Keys.Left))
            {
                if (v2Position.X > (-1575 + fSizePlayer * 125))
                {
                    v2Position.X -= v2Velosity.X;
                }
            }

            // Иммунитет (мигание объекта на 5000 мсек)
            if (isImmunity)
            {
                fTimetImmunity += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                if (colorPlayer.A == 255)
                {
                    isImmunityColourDown = false;
                }
                if (colorPlayer.A == 0)
                {
                    isImmunityColourDown = true;
                }
                if (isImmunityColourDown)
                {
                    colorPlayer.A += 15;
                }
                else
                {
                    colorPlayer.A -= 15;
                }
                if (fTimetImmunity > 5000)
                {
                    isImmunity = false;
                    fTimetImmunity = 0;
                }
            }
            else
            {
                if (colorPlayer.A < 255)
                {
                    colorPlayer = colorPlayerSave;
                }
            }

            fSizePlayer = nMass * 0.1f;
            rRect = new Rectangle((int)v2Position.X, (int)v2Position.Y, t2dTexture.Width, t2dTexture.Height);
            rRectInt = new Rectangle((int)v2Position.X - (int)((250 * fSizePlayer) / 2), (int)v2Position.Y - (int)(250 * fSizePlayer / 2), (int)(250 * fSizePlayer), (int)(250 * fSizePlayer));
            int W = rRectInt.Width / 100 * 30;
            int H = rRectInt.Height / 100 * 30;
            rRectInt.X += W;
            rRectInt.Width -= W * 2;
            rRectInt.Y += H;
            rRectInt.Height -= H * 2;

            v2PositionOriginal = new Vector2(rRect.Width / 2, rRect.Height / 2);
            v2UI = new Vector2(v2Position.X + 100 * fSizePlayer, v2Position.Y + 100 * fSizePlayer);



            //
            //
            //
            //
            //
            //
            // Для отладки
            if (ksKeys.IsKeyDown(Keys.D1)) nMass = 1;
            if (ksKeys.IsKeyDown(Keys.D2)) nMass = 2;
            if (ksKeys.IsKeyDown(Keys.D3)) nMass = 3;
            if (ksKeys.IsKeyDown(Keys.D4)) nMass = 4;
            if (ksKeys.IsKeyDown(Keys.D5)) nMass = 5;
            if (ksKeys.IsKeyDown(Keys.D6)) nMass = 6;
            if (ksKeys.IsKeyDown(Keys.D7)) nMass = 7;
            if (ksKeys.IsKeyDown(Keys.D8)) nMass = 8;
            if (ksKeys.IsKeyDown(Keys.D9)) nMass = 9;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(t2dTexture, v2Position, new Rectangle(0, 0, t2dTexture.Width, t2dTexture.Height), colorPlayer, 0f, v2PositionOriginal, fSizePlayer, SpriteEffects.None, 0.5f);


            //spriteBatch.DrawString(spritefontUI, listTextUI[iCount], new Vector2(v2UI.X, v2UI.Y + nTextUI_Oy), Color.Black, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 0.49f);

        }

        /* Вспомогательные функции */
        public bool Interaction(Rectangle new_rRectInt, bool new_isActiveObj)
        {
            /* Определение соприкосновение объектов */
            if (!new_isActiveObj)
            {
                // Провека существует ли сталкивающийся объект (был ли он ранее съеден)
                return false;
            }
            if (rRectInt.Intersects(new_rRectInt))
            {
                return true;
            }
            return false;
        }

        public bool Versus(int nMassObject)
        {
            /* Сравнение масс ГГ и объекта, выйграет тот у кого масса больше */

            // Провека иммунитета
            if (isImmunity) { return false; }

            // Сравение масс
            if (nMass >= nMassObject)
            {
                if (nMass < 20)
                {
                    nMass++;
                }
                nPointer++;
                return true;
            }
            else
            {
                if (nMass > 1)
                {
                    nMass--;
                    isImmunity = true;
                }
                nLife--;
                return false;
            }
        }

        public void AddLife()
        {
            // -1 жизнь
            nLife++;
        }

        public void Respawn()
        {
            // Возвращение на исходную точку с иммунитетом
            v2Position = new Vector2(0, 0);
            isImmunity = true;
        }
        public void PonterReset()
        {
            nPointer = 0;
        }


        /* Получение private переменных */
        public Vector2 get_v2Position() { return v2Position; }
        public string get_sName() { return sName; }
        public int get_nMass() { return nMass; }
        public int get_nLife() { return nLife; }
        public int get_nPointer() { return nPointer; }
        public void set_nMass(int new_nMass) { nMass = new_nMass; }
        public void set_sName(string new_sName) { sName = new_sName; }
        public void set_colorPlayer(Color new_colorPlayer) {
            colorPlayer = new_colorPlayer;
            colorPlayerSave = colorPlayer;
        }
        public void set_nPointer(int nValue) { nPointer = nValue; }
    }
}
